/*
This example takes range measurements with the VL53L1X and displays additional 
details (status and signal/ambient rates) for each measurement, which can help
you determine whether the sensor is operating normally and the reported range is
valid. The range is in units of mm, and the rates are in units of MCPS (mega 
counts per second).
*/

#include <stdint.h>
#include <avr/pgmspace.h>
#include <Arduino.h>
#include <Wire.h>
#include "VL53L1X.h"
#include "VL53L1X_correction.h"

#define SHUTDOWN_DISTANCE_SENSOR_0 8
#define SHUTDOWN_DISTANCE_SENSOR_1 8
#define SHUTDOWN_DISTANCE_SENSOR_2 8
#define MAX_BUFFER 12

float buffer[MAX_BUFFER] = {0};
VL53L1X sensor;

void init_distance_measurement()
{
    init(); //init arduino
    Serial.begin(9600);
    pinMode(SHUTDOWN_DISTANCE_SENSOR_0, OUTPUT);
    pinMode(SHUTDOWN_DISTANCE_SENSOR_1, OUTPUT);
    pinMode(SHUTDOWN_DISTANCE_SENSOR_2, OUTPUT);
    digitalWrite(SHUTDOWN_DISTANCE_SENSOR_0, LOW);
    digitalWrite(SHUTDOWN_DISTANCE_SENSOR_1, LOW);
    digitalWrite(SHUTDOWN_DISTANCE_SENSOR_2, LOW);
}

int init_distance_sensor()
{
  Wire.begin();
  Wire.setClock(400000);
  sensor.setTimeout(1000);

  if (!sensor.init())
  {
    return 0; 
  } 

  sensor.setDistanceMode(VL53L1X::Short); 
  sensor.setMeasurementTimingBudget(200000);
  return 1; 
}

float read_distance()
{
  sensor.startContinuous(400);
  sensor.read();
  sensor.stopContinuous();
  delay(400);

  if (sensor.ranging_data.range_status != VL53L1X::RangeValid)
  {
    Serial.println("Invalid range");
    return 0.0;
  }

  if(sensor.ranging_data.range_mm < 200 || sensor.ranging_data.range_mm > 515)
  {
    Serial.println("Invalid range");
    return 0.0;
  } 

  int temp = (sensor.ranging_data.range_mm - 200);
  return pgm_read_float_near(&correctedMeasurement[temp]); //return distance with correcion
}

void sample_from_distance_sensors()
{
    static int buffer_i = 0; 
    int sensor_pins[] = {SHUTDOWN_DISTANCE_SENSOR_0, SHUTDOWN_DISTANCE_SENSOR_1, SHUTDOWN_DISTANCE_SENSOR_2}; 
    int i;
    for(i = 0; i < 3; i++)
    {
        digitalWrite(sensor_pins[i], HIGH);
        while(!digitalRead(sensor_pins[i]))
        {
          ; //wait until the pin is actually high
        }

        if(!init_distance_sensor())
        {
            //TODO: error handling 
            Serial.print("error.");
            break;
        }
        buffer[buffer_i] = read_distance();
        buffer_i = (buffer_i + 1) % MAX_BUFFER; 
        digitalWrite(sensor_pins[i], LOW);
    }
}

float calculate_running_average_distance()
{
    sample_from_distance_sensors();
    float sum = 0;
    int i = 0;
    for(i = 0; i < MAX_BUFFER; i++)
    {
        sum += buffer[i];
    }

    return sum / MAX_BUFFER; 
}

int main(void)
{
  init_distance_measurement();

  while(1)
  {
    Serial.print("the distance is ");
    Serial.print(calculate_running_average_distance()); 
    Serial.print(" mm\n"); 
    Serial.print("the buffer contains ");
    int i = 0;
    for(i = 0; i < MAX_BUFFER; i++)
    {
        Serial.print(buffer[i]);
        Serial.print(" ");
    }
    Serial.print("\n");
  }

  return 0;
}