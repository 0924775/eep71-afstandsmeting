# AR Babylengtemeter - afstandmeting
Deze repository bevat alle code rondom de afstandsmeting van de AR babylengtemeter die gebouwd is tijdens EEP71 2019/2020.

## Quickstart - schuifmaat 
De schuifmaat oplossing is gebouwd met `gcc` voor de `Raspberry Pi 3(B)`. Het is mogelijk om de code te runnen in alle Raspberry Pi omgevingen die `wiringPi` ondersteunen. Als er wordt gewerkt met een reguliere Raspberry Pi kan de schuifmaat niet direct worden aangesloten maar moet dit worden gedaan zoals aangegeven op het onderstaande schema. 

![Schematische tekening voor het aansluiten van de schuifmaat](./schematic-schuifmaat.png)

Pinnen moeten in de code worden aangepast en in de `setup` functie moet `wiringPiSetupGpio()` worden veranderd naar `wiringPiSetup()`. Door `make` in de map schuifmaat uit te voeren op een raspberruy pi kan de library opnieuw worden gecompileerd. Als de library niet wil compileren check dan of wiringPi is geinstaleerd door `gpio -v` te runnen in de terminal. De library is te installeren door `sudo apt-get install wiringpi` te runnen.

Een voorbeeld van hoe de library gebruikt kan worden in python is te vinden in het bestand `example.py`.

## Quickstart - TOF
De TOF oplossing is gebouwd in `vscode` met de `platformIO` extensie, het is aan te raden om de code door te ontwikkelen of te flashen in dezelfde omgeving. De code in deze repository maakt gebruikt van correctie v4. De sparkfun breakout kan direct worden aangesloten op een arduino of ATmega328p. 